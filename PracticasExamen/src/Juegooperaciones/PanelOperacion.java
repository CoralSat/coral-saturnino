package Juegooperaciones;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class PanelOperacion extends javax.swing.JPanel {
    public static int vidas;
    public static int aciertos;
    private HiloSegundero hiloSegundero;
    private char operadores[] = {'*', '+', '-', '/'};
    public PanelOperacion() {
        initComponents();
        txtResultado.setVisible(false);
        btnValidar.setVisible(false);
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        num1 = new javax.swing.JLabel();
        ope = new javax.swing.JLabel();
        num2 = new javax.swing.JLabel();
        igual = new javax.swing.JLabel();
        txtResultado = new javax.swing.JTextField();
        btnValidar = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        btnComenzar = new javax.swing.JButton();

        setLayout(new java.awt.BorderLayout(0, 20));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("Calcula la Operación");
        jPanel1.add(jLabel1);

        add(jPanel1, java.awt.BorderLayout.NORTH);

        jPanel2.add(num1);
        jPanel2.add(ope);
        jPanel2.add(num2);
        jPanel2.add(igual);

        txtResultado.setColumns(10);
        jPanel2.add(txtResultado);

        btnValidar.setText("Validar");
        btnValidar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnValidarActionPerformed(evt);
            }
        });
        jPanel2.add(btnValidar);

        add(jPanel2, java.awt.BorderLayout.CENTER);

        btnComenzar.setText("Comenzar");
        btnComenzar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnComenzarActionPerformed(evt);
            }
        });
        jPanel3.add(btnComenzar);

        add(jPanel3, java.awt.BorderLayout.PAGE_END);
    }// </editor-fold>//GEN-END:initComponents

    private void btnComenzarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnComenzarActionPerformed
        if(txtResultado.isVisible()) return;
        aciertos = 0;
        vidas = 3;
        JOptionPane.showMessageDialog(null, "Se ha iniciado el juego, tienes 3 Vidas");
        hiloSegundero = new HiloSegundero();
        hiloSegundero.start();
        txtResultado.setVisible(true);
        btnValidar.setVisible(true);
        crearOperacion();
    }//GEN-LAST:event_btnComenzarActionPerformed

    private void btnValidarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnValidarActionPerformed
        if(txtResultado.getText().isEmpty()) return;
        hiloSegundero.destruir();
        int N = Integer.parseInt(num1.getText());
        int M = Integer.parseInt(num2.getText());
        int R = Integer.parseInt(txtResultado.getText());
        String op = ope.getText();
        switch(op){
            case "+":
                if(N + M == R){
                    JOptionPane.showMessageDialog(null, "Resultado correcto\nAciertos: " + (aciertos + 1));
                    aciertos++;
                    crearOperacion();
                }else{
                    JOptionPane.showMessageDialog(null, "Resultado incorrecto, haz perdido una vida\nVida Restantes: " + (vidas - 1));
                    vidas--;
                    txtResultado.setText("");
                }
                break;
            case "-":
                if(N - M == R){
                    JOptionPane.showMessageDialog(null, "Resultado correcto\nAciertos: " + (aciertos + 1));
                    aciertos++;
                    crearOperacion();
                }else{
                    JOptionPane.showMessageDialog(null, "Resultado incorrecto, haz perdido una vida\nVida Restantes: " + (vidas - 1));
                    vidas--;
                    txtResultado.setText("");
                }
                break;
                
            case "*":
                if(N * M == R){
                    JOptionPane.showMessageDialog(null, "Resultado correcto\nAciertos: " + (aciertos + 1));
                    aciertos++;
                    crearOperacion();
                }else{
                    JOptionPane.showMessageDialog(null, "Resultado incorrecto, haz perdido una vida\nVida Restantes: " + (vidas - 1));
                    vidas--;
                    txtResultado.setText("");
                }
                break;
            case "/":
                if(N / M == R){
                    JOptionPane.showMessageDialog(null, "Resultado correcto\nAciertos: " + (aciertos + 1));
                    aciertos++;
                    crearOperacion();
                }else{
                    JOptionPane.showMessageDialog(null, "Resultado incorrecto, haz perdido una vida\nVida Restantes: " + (vidas - 1));
                    vidas--;
                    txtResultado.setText("");
                }
                break;
        }
        if(vidas == 0){
            JOptionPane.showMessageDialog(null, "Haz perdido, ya no te quedan vidas.\nTotal de Aciertos: " + aciertos);
            num1.setText("");
            num2.setText("");
            ope.setText("");
            igual.setText("");
            txtResultado.setVisible(false);
            btnValidar.setVisible(false);
            return;
        }
        hiloSegundero = new HiloSegundero();
        hiloSegundero.start();
    }//GEN-LAST:event_btnValidarActionPerformed

    
    public void crearOperacion(){
        int N = (int) (Math.random() * 9) + 1;
        int M = (int) (Math.random() * 9) + 1;
        int OP = (int) (Math.random() * 3);
        if(OP == 3){
            while(N % M == 0){
                N = (int) (Math.random() * 9) + 1;
                M = (int) (Math.random() * 9) + 1;
            }
        }
        num1.setText(N + "");
        num2.setText(M + "");
        ope.setText(operadores[OP] + "");
        igual.setText("=");
        txtResultado.setText("");
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnComenzar;
    public static javax.swing.JButton btnValidar;
    public static javax.swing.JLabel igual;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    public static javax.swing.JLabel num1;
    public static javax.swing.JLabel num2;
    public static javax.swing.JLabel ope;
    public static javax.swing.JTextField txtResultado;
    // End of variables declaration//GEN-END:variables
}
